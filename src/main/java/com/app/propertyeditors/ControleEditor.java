package com.app.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.app.model.Annee;
import com.app.model.Controle;



public class ControleEditor extends PropertyEditorSupport {
	
	// Converts a String to a Permission (when submitting form)
    @Override
    public void setAsText(String text) {
    	int id=Integer.valueOf(text);
    	Controle c=new Controle();
    	c.setId(id);
        this.setValue(c);
    }

    // Converts a Permission to a String (when displaying form)
    public String getAsText() {
    	if(getValue() == null){
    	    return null;
    	   }
    	Controle c = (Controle) this.getValue();
    	if (c != null)
            return c.getIntitule();
        return null;
    }

}
