<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>


<%! String menuActuel = "menu_etudiants";  %>
<%! String sousMenuActuel = "menu_etudiants_emploies";  %>

<jsp:include page="../../layout/header.jsp" />
<jsp:include page="../../layout/leftpanel.jsp" />
<jsp:include page="../../layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-graduation-cap"></i> Étudiants <span>Emploie du temps</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="#.">Étudiants</a></li>
          <li class="active">Emploie du temps</li>
        </ol>
      </div>
    </div>
    
   
        	<div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="panel-close">&times;</a>
            <a href="#" class="minimize">&minus;</a>
          </div><!-- panel-btns -->
          <h3 class="panel-title"><a href="<c:url value="/etudiants/emploie/ajouter" />" class="btn btn-primary-alt"><i class="fa fa-plus"></i> Ajouter</a> </h3>
        </div>
        <div class="panel-body">
	            
	            
	            <c:if test="${success != null}">
					<div class="alert alert-success" role="alert">
						<strong>Well done!</strong> ${success}
					</div>
				</c:if>
				
				<c:if test="${error != null}">
					<div class="alert alert-danger" role="alert">
						<strong>Oh snap!</strong> ${error}
					</div>
				</c:if>
	            
	            <c:if test="${success_emploie_delete != null}">
					<div class="alert alert-success" role="alert">
						<strong>Well done!</strong> ${success_emploie_delete}
					</div>
				</c:if>
				
				<c:if test="${error_emploie_delete != null}">
					<div class="alert alert-danger" role="alert">
						<strong>Oh snap!</strong> ${error_emploie_delete}
					</div>
				</c:if>
				
				<hr />
		
				<div class="table-responsive">
	            <table class="table" id="table1">
	              <thead>
	                 <tr>
	                 	<th class="no-sort">&amp;</th>
	                 	<th class="no-sort"><span class="glyphicon glyphicon-folder-close"></span></th>
	                    <th>Intitulé</th>
	                    <th>Formation</th>
	                    <th>Filiere</th>
	                    <th>Niveaux </th>
	                    <th>Semestre</th>
	                    <th>Date Création</th>
	                   
	                 </tr>
	              </thead>
	              <tbody>
	              <c:forEach var="v" items="${liste_emploies}">
								<tr>
         							<td><a href="<c:url value="/etudiants/emploie/deleteEmploie?id=${v.id}"/>" class="column-delete delete" style="  color: #D9534F;"><i class="fa fa-trash-o"></i></a> </td>
                                    <td><a href="<c:url value="/etudiants/emploie/dowload?file=${v.path}" />"><span class="glyphicon glyphicon-export" title="Télécharger"></span></a></td>
                                    <td><a href="<c:url value="/etudiants/emploie/modifier?id=${v.id}" />">${v.intitule}</a></td>
                                    <td>${v.formation.intitule}</td>
                                    <td>${v.filiere.intitule}</td>
                                    <td>${v.niveau.intitule}</td>
                                    <td>${v.semestre.intitule}</td>
                                    <td><joda:format pattern="MM/dd/yyyy" value="${v.date_creation}" /></td>
                                 </tr>
                  </c:forEach>
	              </tbody>
              </table>
    
	   </div>
	  </div>
	  </div>

        
<jsp:include page="../../layout/rightpanel.jsp" />
<jsp:include page="../../layout/footer.jsp" />
<script src="<c:url value="/assets/js/chosen.jquery.min.js" />"></script>

<script>

jQuery(document).ready(function() {
	
    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");
    
    jQuery('#table1').dataTable({
        "sPaginationType": "full_numbers",
        "aoColumnDefs" : [ {
    	    "bSortable" : false,
    	    "aTargets" : [ "no-sort" ]
    	} ]
      });
    
    // Chosen Select
    jQuery("select").chosen({
      'min-width': '100px',
      'white-space': 'nowrap',
      disable_search_threshold: 10
    });
    
    jQuery("a.delete").click(function(e){
	  	  e.preventDefault();
	  	  var url = $(this).attr("href");
	      swal(
	      {
	          title: "Êtes-vous sure?",
	          text: "Vous ne serez pas en mesure de récupérer cet élément",
	          type: "warning",
	          showCancelButton: true,
	          confirmButtonColor: "#DD6B55",
	          confirmButtonText: "Oui, supprimez-le!",
	          cancelButtonText: "Non, annuler!",
	          closeOnConfirm: false,
	          closeOnCancel: false
	      },
	      function(isConfirm)
	      {
	          if (isConfirm) {
	          	window.location = url;
	          	swal("Suppression!", "L'élement va être supprimé dans quelques instants.", "success");
	          }
	          else {
	              swal("Annulé", "Aucune opération n'a été effectuer", "error");
	          }
	      });
	      
	      return false;
	    });

  });

</script>

</body>
</html>