<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>


<%! String menuActuel = "menu_dashboard";  %>


<jsp:include page="layout/header.jsp" />
<jsp:include page="layout/leftpanel.jsp" />
<jsp:include page="layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-home"></i> GoFormation <span>Tableau de bord</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="/" />">GoFormation</a></li>
          <li class="active">Tableau de bord</li>
        </ol>
      </div>
    </div>

<div class="contentpanel">
    

<div id="timeline-list" class="row" style="max-width: 950px;">

	<div class="col-sm-6">
            <div class="panel panel-dark panel-alt timeline-post">
                <div class="panel-body">  
                <br>            
                    <p class="text-muted text-center">Prenez un Selfie <i class="fa fa-instagram text-success"></i> et partager le avec nous</p>
                </div><!-- panel-body -->
                <div class="panel-footer">
                    <div class="timeline-btns pull-left" style="padding: 0;">
                        <a href="" class="btn btn-success-alt tooltips" style="width: auto;height: auto;" data-toggle="tooltip" title="Android"><i class="fa fa-android"></i></a>
                        <a href="" class="btn btn-info-alt tooltips" style="width: auto;height: auto;" data-toggle="tooltip" title="Windows Phone"><i class="fa fa-windows"></i></a>
                        <a href="" class="btn btn-default-alt tooltips" style="width: auto;height: auto;" data-toggle="tooltip" title="iOS"><i class="fa fa-apple"></i></a>
                    </div><!--timeline-btns -->
                    <a href="#." class="btn btn-success pull-right">Made with love <i class="fa fa-heartbeat"></i> </a>
                </div><!-- panel-footer -->
            </div>
        </div><!-- col-sm-6 -->
        
        
        <c:forEach var="v" items="${selfies}">
        <div class="col-sm-6">
          <div class="panel panel-default panel-timeline">
            
            <div class="panel-body" style="padding-top: 10px;padding-bottom: 10px;">
                <img src="${v.image }" class="img-responsive" alt="" />
                <div class="timeline-btns">
                    <div class="pull-left">
                    
                        <a href="" class="btn btn-primary-alt tooltips" data-toggle="tooltip" title="Partager sur Facebook" style="width: 30px;padding: 4px;height: 30px;"><i class="fa fa-facebook"></i></a>
                        <a href="" class="btn btn-danger-alt tooltips" data-toggle="tooltip" title="Partager sur Google Plus" style="width: 30px;padding: 4px;height: 30px;"><i class="fa fa-google-plus"></i></a>
                        <a href="" class="btn btn-info-alt tooltips" data-toggle="tooltip" title="Partager sur Twitter" style="width: 30px;padding: 4px;height: 30px;"><i class="fa fa-twitter"></i></a>
                        <a href="" class="btn btn-warning-alt tooltips" data-toggle="tooltip" title="Partager sur Instagram" style="width: 30px;padding: 4px;height: 30px;"><i class="fa fa-instagram"></i></a>
                    
                    </div>
                    <div class="pull-right">
                        <small class="text-muted" style="  position: relative;
  top: 6px;"><i class="fa fa-clock-o"></i> <joda:format pattern="MM/dd/yyyy h:m:s" value="${v.dateop }" /></small>      
                    </div>
                </div>
                
               
                
            </div><!-- panel-body -->
            <div class="panel-footer">
            <p>
                ${v.comment }
                </p>
            </div>
           
          </div><!-- panel -->
        </div><!-- col-sm-6 -->
        </c:forEach>
        
        
        
        
        

</div>
    
   

  
</div><!-- ./contentpanel -->
    
<jsp:include page="layout/rightpanel.jsp" />
<jsp:include page="layout/footer.jsp" />
<!-- 
<script src="<c:url value="/assets/js/flot/jquery.flot.min.js" />"></script>
<script src="<c:url value="/assets/js/flot/jquery.flot.resize.min.js" />"></script>
<script src="<c:url value="/assets/js/flot/jquery.flot.spline.min.js" />"></script>
 -->
<script src="<c:url value="/assets/js/chosen.jquery.min.js" />"></script>
<script src="<c:url value="/assets/js/masonry.pkgd.min.js" />"></script>
<script>
  jQuery(document).ready(function() {
    jQuery("a#<%= menuActuel %>").parent("li").addClass("active");
    var container = document.querySelector('#timeline-list');
    var msnry = new Masonry( container, {
      // options
      columnWidth: '.col-sm-6',
      itemSelector: '.col-sm-6'
    });
    
    // check on load
    if(jQuery(window).width() <= 640 )
        msnry.destroy();

    // check on resize
    jQuery(window).resize(function(){
        if(jQuery(this).width() <= 640 )
            msnry.destroy();
    });
    
  });
 
</script>

</body>
</html>