package com.app.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.model.Enseignant;
import com.app.model.Etudiant;
import com.app.model.Filiere;
import com.app.model.Selfie;
import com.app.service.EnseignantService;
import com.app.service.EtudiantService;
import com.app.service.FiliereService;
import com.app.service.SelfieService;


@Controller
public class ApiController {
	@Autowired
	EnseignantService enseignantService;
	@Autowired
	EtudiantService etudiantService;
	@Autowired
	FiliereService filiereService;
	@Autowired
	SelfieService selfieService;
	
	
	@RequestMapping(value = "/api/professeurs", method = RequestMethod.GET)
	public @ResponseBody List<Enseignant> professeurs() 
	{
		return enseignantService.getAll();
	}
	@RequestMapping(value = "/api/professeur", method = RequestMethod.GET)
	public @ResponseBody Enseignant professeur(@RequestParam(value = "id", required = true, defaultValue = "0") int id) 
	{
		return enseignantService.findById(id);
	}
	
	@RequestMapping(value = "/api/professeurImage", method = RequestMethod.GET)
	public @ResponseBody  void consulter(
			@RequestParam(value = "path", required = true, defaultValue = "") String path,
			HttpServletResponse response
			) {
		
		String rootPath = System.getProperty("catalina.base");
		File dir = new File(rootPath + File.separator + "uploads/enseignant");;
		
		
		 try
	        {
	            // get your file as InputStream
			 System.out.println(dir.getAbsolutePath()
				        + File.separator + path);
	            InputStream is = new FileInputStream(dir.getAbsolutePath()
				        + File.separator + path);
	            // copy it to response's OutputStream
	            IOUtils.copy( is, response.getOutputStream() );
	            response.flushBuffer();
	        }
	        catch( IOException ex )
	        {
	            throw new RuntimeException( "IOError writing file to output stream" );
	        }
	
	}
	
	@RequestMapping(value = "/api/etudiants", method = RequestMethod.GET)
	public @ResponseBody List<Etudiant> etudiants() 
	{
		return etudiantService.getAll();
	}
	
	@RequestMapping(value = "/api/etudiant", method = RequestMethod.GET)
	public @ResponseBody Etudiant etudiant(@RequestParam(value = "id", required = true, defaultValue = "0") int id) 
	{
		return etudiantService.findById(id);
	}
	
	@RequestMapping(value = "/api/filieres", method = RequestMethod.GET)
	public @ResponseBody List<Filiere> filieres() 
	{
		return filiereService.getAll();
	}
	
	
	@RequestMapping(value = "/api/takeselfie", method = RequestMethod.POST)
	public @ResponseBody Selfie selfie(
			@RequestBody Selfie s 
	) 
	{
		/*Selfie s = new Selfie();
		s.setComment(comment);
		s.setImage(image);*/
		
		selfieService.add(s);
		
		return s;
	}
	
	@RequestMapping(value = "/api/selfies", method = RequestMethod.GET)
	public @ResponseBody List<Selfie> selfies() 
	{
		return selfieService.getAll();
	}
	
	
	

}
