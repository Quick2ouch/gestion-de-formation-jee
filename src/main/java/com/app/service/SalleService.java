package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Salle;

public class SalleService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Salle> getAll() {
		List<Salle> result = em.createQuery("SELECT p FROM Salle p",
				Salle.class).getResultList();
		return result;
	}
	
	@Transactional
	public void add(Salle p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Salle p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Salle update(Salle p) throws NotFoundException{
		Salle up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Salle findById(int id) {
		return em.find(Salle.class, id);
	}
}