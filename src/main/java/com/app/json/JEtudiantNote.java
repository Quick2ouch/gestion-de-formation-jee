package com.app.json;

public class JEtudiantNote {
	private String etudiant;
	private int etudiant_id;
	
	private Double note;
	public JEtudiantNote(String etudiant,int etudiant_id, Double note) {
		super();
		this.etudiant = etudiant;
		this.etudiant_id = etudiant_id;
		this.note = note;
	}
	public JEtudiantNote() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getEtudiant() {
		return etudiant;
	}
	public void setEtudiant(String etudiant) {
		this.etudiant = etudiant;
	}
	public Double getNote() {
		return note;
	}
	public void setNote(Double note) {
		this.note = note;
	}
	public int getEtudiant_id() {
		return etudiant_id;
	}
	public void setEtudiant_id(int etudiant_id) {
		this.etudiant_id = etudiant_id;
	}

	
	
	
}
