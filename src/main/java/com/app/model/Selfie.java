package com.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name = "selfies")
public class Selfie {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Type(type="text")
	private String image;
	@Type(type="text")
	private String comment;
	
	@Column
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime dateop;

	public Selfie() {
		super();
		dateop = new DateTime();
	}

	public Selfie(int id, String image, String comment, DateTime dateop) {
		super();
		this.id = id;
		this.image = image;
		this.comment = comment;
		this.dateop = dateop;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public DateTime getDateop() {
		return dateop;
	}

	public void setDateop(DateTime dateop) {
		this.dateop = dateop;
	}

	@Override
	public String toString() {
		return "Selfie [id=" + id + ", image=" + image + ", comment=" + comment
				+ ", dateop=" + dateop + "]";
	}
	
	
	
	
}
