<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<%! String menuActuel ="e_r";  %>


<jsp:include page="../../../views/layout/header.jsp">
	<jsp:param name="stylesheets" value="/assets/css/jquery.gritter.css" />
</jsp:include>
<jsp:include page="../../../views/layout/leftpanel.jsp" />
<jsp:include page="../../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-users"></i> Students <span>Retards</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="etudiants" />"> Students </a></li>
          <li class="active">Retards</li>
        </ol>
      </div>
    </div>
    
     <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="panel-close">&times;</a>
            <a href="#" class="minimize">&minus;</a>
          </div><!-- panel-btns -->
          
        </div>
        <div class="panel-body">
        <c:if test="${success_delete != null}">
			<div class="alert alert-success" role="alert">
				<strong>Well done!</strong> ${success_delete}
			</div>
		</c:if>
		
		<c:if test="${error_delete != null}">
			<div class="alert alert-danger" role="alert">
				<strong>Oh snap!</strong> ${error_delete}
			</div>
		</c:if>
		
        	<div class="table-responsive">
	            <table class="table" id="table1">
	              <thead>
	                 <tr>
	                 	
	                    
	                    <th>Type de séance</th>
	                    <th>Matière</th>
	                    <th>Date</th>
	                    <th><i class="fa fa-clock-o"></i> Début</th>
	                    <th><i class="fa fa-clock-o"></i> Fin</th>
	                    <th>Jutifié?</th>
	                    <th class="no-sort">Justification</th>
	                 </tr>
	              </thead>
	              <tbody>
	              <c:forEach var="v" items="${liste}">
								<tr>
         							
                                  
                                    <td>${v.typeSeance}</td>
                                    <td>${v.matiere.intitule}</td>
                                    <td><joda:format pattern="MM/dd/yyyy" value="${v.dateSeance}" /></td>
                                    <td>${v.heureDebut}</td>
                                    <td>${v.heureFin}</td>
                                     <c:if test="${v.justifier==true}"> 
                                     <td><span class="label label-success">OUI</span> </td>
                                     
                                     </c:if>
                                     <c:if test="${v.justifier==false}"> 
                                     <td><span class="label label-danger">NON</span> </td>
                                     
                                     </c:if>
                                     <td id="justifier-column-${v.id}">${v.justification}	</td>
                                    
                                </tr>
					</c:forEach>
								
	                 
	              </tbody>
	             </table>
             </div>
        </div>
     </div>
     
     
<c:forEach var="v" items="${liste}">
<div class="modal fade" id="justification-modal-${v.id}" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
        <form action="#." method="post" id="justification-form-${v.id}" data-id="${v.id}">
	        <div class="panel panel-dark panel-alt">
			    <div class="panel-heading">
			        <div class="panel-btns">
			            <a  aria-hidden="true" data-dismiss="modal" class="close">&times;</a>
			        </div><!-- panel-btns -->
			        <h3 class="panel-title">Justification</h3>
			        <p>Veuillez entrer une justification pour cette absence</p>
			    </div>
			    <div class="panel-body">
			         <div class="form-group">
			              <div class="col-sm-12">
			                <textarea class="form-control" name="message" rows="5"></textarea>
			              </div>
			            </div>
			    </div>
			    <div class="panel-footer text-right">
			    	<button type="button" class="btn btn-success submit-justification-form" data-id="${v.id}">Valider</button>
			    	<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
			    </div>
			</div>
        </form>
        
    </div>
    
  </div>
</div>
 </c:forEach>
 
 
<jsp:include page="../../../views/layout/rightpanel.jsp" />
<jsp:include page="../../../views/layout/footer.jsp" />

<script src="<c:url value="/assets/js/chosen.jquery.min.js" />"></script>
<script src="<c:url value="/assets/js/jquery.gritter.min.js" />"></script>

<script>


jQuery(document).ready(function() {
	    jQuery("a#<%= menuActuel %>").parent("li").addClass("active");

  jQuery('#table1').dataTable({
      "sPaginationType": "full_numbers"
    });
  
// Chosen Select
  jQuery("select").chosen({
    'min-width': '100px',
    'white-space': 'nowrap',
    disable_search_threshold: 10
  });

});

</script>

</body>
</html>