<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<%! String menuActuel = "menu_module";  %>
<%! String sousMenuActuel = "menu_module_matieres";  %>

<jsp:include page="../../../views/layout/header.jsp" />
<jsp:include page="../../../views/layout/leftpanel.jsp" />
<jsp:include page="../../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-suitcase"></i> Modules <span>Matieres</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="module" />">Modules</a></li>
          <li class="active">Matieres</li>
        </ol>
      </div>
    </div>
    
     <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="panel-close">&times;</a>
            <a href="#" class="minimize">&minus;</a>
          </div><!-- panel-btns -->
          <h3 class="panel-title"><a href="<c:url value="/module/matieres/ajouter" />" class="btn btn-primary-alt"><i class="fa fa-plus"></i> Ajouter</a> </h3>
        </div>
        <div class="panel-body">
        <c:if test="${success_matiere_delete != null}">
							<div class="alert alert-success" role="alert">
								<strong>Well done!</strong> ${success_matiere_delete}
							</div>
						</c:if>
						
						<c:if test="${error_matiere_delete != null}">
							<div class="alert alert-danger" role="alert">
								<strong>Oh snap!</strong> ${error_matiere_delete}
							</div>
						</c:if>
						
        	<div class="table-responsive">
	            <table class="table" id="table1">
	              <thead>
	                 <tr>
	                    <th> Intitulé</th>
                                <th>Année</th>
                                  <th>Filière</th>
                                   <th>Niveaux</th>
                                    <th>Semestre</th>
                                     <th>Module</th>
                                      <th>Coefficient</th>
                                		<th>Enseignant</th>
                      </tr>
	              </thead>
	              <tbody>
	              <c:forEach var="v" items="${liste_matieres}">
								     <tr>
								    <td><a href="matieres/editMatiere?id=${v.id}">${v.intitule}</a></td>
                                    <td>${v.annee.intitule}</td>
                                    <td>${v.filiere.intitule}</td>
                                    <td>${v.niveaux.intitule}</td>
                                    <td>${v.semestre.intitule}</td>
                                    <td>${v.module.contextuel}</td>
                                    <td>${v.coeff}</td>
                                    
                                     <td>
                                     <c:choose>
									      <c:when test="${v.enseignant == null }">
									      <span class="label label-danger">Non Affecté </span>
  									      </c:when>
									      <c:otherwise>
									      	${v.enseignant.nom} ${v.enseignant.prenom}
									      </c:otherwise>
									</c:choose>
									</td>
                                    
                                    
                                    
                                    
                                   
                                   
                                </tr>
					</c:forEach>
								
	                 
	              </tbody>
	             </table>
             </div>
        </div>
     </div>
 
<jsp:include page="../../../views/layout/rightpanel.jsp" />
<jsp:include page="../../../views/layout/footer.jsp" />

<script src="<c:url value="/assets/js/chosen.jquery.min.js" />"></script>

<script>
  jQuery(document).ready(function() {

    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");
    
    
    jQuery('#table1').dataTable({
        "sPaginationType": "full_numbers"
      });
    
 // Chosen Select
    jQuery("select").chosen({
      'min-width': '100px',
      'white-space': 'nowrap',
      disable_search_threshold: 10
    });

  });
</script>

</body>
</html>