package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Selfie;

public class SelfieService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Selfie> getAll() {
		List<Selfie> result = em.createQuery("SELECT p FROM Selfie p ORDER BY id DESC",
				Selfie.class).getResultList();
		return result;
	}
	
	@Transactional
	public Selfie add(Selfie p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		em.flush();
		return p;
	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Selfie p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Selfie update(Selfie p) throws NotFoundException{
		Selfie up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Selfie findById(int id) {
		return em.find(Selfie.class, id);
	}
}