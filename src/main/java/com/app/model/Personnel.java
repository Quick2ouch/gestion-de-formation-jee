package com.app.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;

@Entity
@DiscriminatorValue("personnel")
public class Personnel extends Person{

	@NotEmpty(message = "Entrez une fonction.")
	private String fonction;
	
	
	public Personnel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Personnel(int id, String prenom, String nom, String sexe,
			String cin, String telephone, String email, DateTime date_entree,
			DateTime date_sortie, String banque, String rib, int type_paiement, String fonction) {
		super(id, prenom, nom, sexe, cin, telephone, email, date_entree, date_sortie,
				banque, rib, type_paiement);
		this.fonction = fonction;
	}

	public String getFonction() {
		return fonction;
	}

	public void setFonction(String fonction) {
		this.fonction = fonction;
	}

	@Override
	public String toString() {
		return "Personnel [fonction=" + fonction + ", toString()="
				+ super.toString() + "]";
	}

	

	

	
	
	
}
