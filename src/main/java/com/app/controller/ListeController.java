package com.app.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.model.User;
import com.app.propertyeditors.UserEditor;
import com.app.service.EnseignantService;
import com.app.service.LogTrackerService;
import com.app.service.MatiereService;
import com.app.service.UserService;

@Controller
public class ListeController {
	@Autowired
	LogTrackerService logSvc;
	@Autowired
	EnseignantService enseignantService;
	@Autowired
	MatiereService matieretService;
	@Autowired
	UserService userService;
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(User.class, new UserEditor());
    }
	
	@PreAuthorize("hasRole('ETUDIANT')")
	@RequestMapping(value = "/student/professeurs", method = RequestMethod.GET)
	public String index(Model model) 
	{
		model.addAttribute("liste", enseignantService.getAll());
		return "professeurs/liste";
	}
	
	@RequestMapping(value = "/student/professeurs/consulter", method = RequestMethod.GET)
	public @ResponseBody  void consulter(
			@RequestParam(value = "path", required = true, defaultValue = "") String path,
			HttpServletResponse response
			) {
		
		String rootPath = System.getProperty("catalina.base");
		File dir = new File(rootPath + File.separator + "uploads/enseignant");;
		
		
		 try
	        {
	            // get your file as InputStream
			 System.out.println(dir.getAbsolutePath()
				        + File.separator + path);
	            InputStream is = new FileInputStream(dir.getAbsolutePath()
				        + File.separator + path);
	            // copy it to response's OutputStream
	            IOUtils.copy( is, response.getOutputStream() );
	            response.flushBuffer();
	        }
	        catch( IOException ex )
	        {
	            throw new RuntimeException( "IOError writing file to output stream" );
	        }
	
	}
	
	
	
	

}
