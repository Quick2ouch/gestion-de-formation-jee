package com.app.service;

//import java.sql.Date;
import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Personnel;
import com.app.model.User;

public class PersonnelService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Personnel> getAll() {
		List<Personnel> result = em.createQuery("SELECT p FROM Personnel p",
				Personnel.class).getResultList();
		return result;
	}

	@Transactional
	public void add(Personnel p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Personnel p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Personnel update(Personnel p) throws NotFoundException{
		Personnel up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Personnel findById(int id) {
		return em.find(Personnel.class, id);
	}
	
	
}