package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Attestation;

public class AttestationService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Attestation> getAll() {
		List<Attestation> result = em.createQuery("SELECT p FROM Attestation p",
				Attestation.class).getResultList();
		return result;
	}

	@Transactional
	public Attestation add(Attestation p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		return p;


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Attestation p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Attestation update(Attestation p) throws NotFoundException{
		Attestation up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Attestation findById(int id) {
		return em.find(Attestation.class, id);
	}
}