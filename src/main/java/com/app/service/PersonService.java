package com.app.service;

//import java.sql.Date;
import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Person;

public class PersonService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Person> getAll() {
		List<Person> result = em.createQuery("SELECT p FROM Person p",
				Person.class).getResultList();
		return result;
	}

	@Transactional
	public void add(Person p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Person p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Person update(Person p) throws NotFoundException{
		Person up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Person findById(int id) {
		return em.find(Person.class, id);
	}
}