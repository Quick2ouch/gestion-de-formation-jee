<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>


<%! String menuActuel = "menu_module";  %>
<%! String sousMenuActuel = "menu_module_modules";  %>


<jsp:include page="../../../views/layout/header.jsp" />
<jsp:include page="../../../views/layout/leftpanel.jsp" />
<jsp:include page="../../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-users"></i> Module<span>Modules</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="employees" />">Module</a></li>
          <li class="active">Modules</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
     <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="panel-close">&times;</a>
            <a href="#" class="minimize">&minus;</a>
          </div><!-- panel-btns -->
          <h3 class="panel-title">Modification "${moduleForm.intitule}"</h3>
        </div>
        <f:form method="post" action="updatemodule" modelAttribute="moduleForm">
        <f:hidden path="id" />
			
        <div class="panel-body">
        <c:if test="${success_module != null}">
							<div class="alert alert-success" role="alert">
								<strong>Well done!</strong> ${success_module}
							</div>
						</c:if>
						
						<c:if test="${error_module != null}">
							<div class="alert alert-danger" role="alert">
								<strong>Oh snap!</strong> ${error_module}
							</div>
						</c:if>
			

        	 <div class="row">
             <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        	
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Intitulé <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <f:input path="intitule" cssClass="form-control" size="20"/>
                               	<f:errors path="intitule" cssClass="errors"></f:errors>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-sm-3 control-label">Coefficient <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <f:input path="coefficient" cssClass="form-control" size="20"/>
                               	<f:errors path="coefficient" cssClass="errors"></f:errors>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-sm-3 control-label">Contextuel <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <f:input path="contextuel" cssClass="form-control" size="20"/>
                               	<f:errors path="contextuel" cssClass="errors"></f:errors>
                            </div>
                        </div>
                      
                         <div class="form-group">
                    <label class=" col-sm-3 control-label">Centre </label>
                    <div class="col-sm-8">
                    <f:select path="centre" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UN CENTRE--</f:option>
					    <f:options items="${liste_centre}" itemValue="id" itemLabel="intitule" />
					</f:select>
					</div>
                    <f:errors path="centre" cssClass="error"></f:errors>
                  </div>
                   </div>
                   <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                    <label class=" col-sm-3 control-label">Annee </label>
                    <div class="col-sm-8">
                    <f:select path="annee" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UNE ANNEE--</f:option>
					    <f:options items="${liste_annee}" itemValue="id" itemLabel="intitule" />
					</f:select>
					</div>
                    <f:errors path="annee" cssClass="error"></f:errors>
                  </div>
                   
                   
                   <div class="form-group">
                    <label class=" col-sm-3 control-label">Formation </label>
                    <div class="col-sm-8">
                    <f:select path="formation" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UNE FORMATION--</f:option>
					    <f:options items="${liste_formation}" itemValue="id" itemLabel="intitule" />
					</f:select>
					</div>
                    <f:errors path="formation" cssClass="error"></f:errors>
                  </div>
                    <div class="form-group">
                    <label class=" col-sm-3 control-label">Filiere</label>
                    <div class="col-sm-8">
                    <f:select path="filiere" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UNE FILIERE--</f:option>
					    <f:options items="${liste_filiere}" itemValue="id" itemLabel="intitule" />
					</f:select>
					</div>
                    <f:errors path="filiere" cssClass="error"></f:errors>
                  </div>
                   <div class="form-group">
                    <label class="col-sm-3 control-label">Niveau</label>
                    <div class="col-sm-8">
                    <f:select path="niveau" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UN NIVEAU--</f:option>
					    <f:options items="${liste_niveaux}" itemValue="id" itemLabel="intitule" />
					</f:select>
					</div>
                    <f:errors path="niveau" cssClass="error"></f:errors>
                  </div>
                   <div class="form-group">
                            <label class="col-sm-3 control-label">Semestre <span class="asterisk">*</span></label>
                            <div class="col-sm-8">
                                <f:select path="semestre" class="form-control chosen-select">
			                    	<f:option value="0"> --SELECTIONNER UN SEMESTRE--</f:option>
								    <f:options items="${semestre}" itemValue="id" itemLabel="intitule" />
								</f:select>
                               	<f:errors path="semestre" cssClass="error"></f:errors>
                            </div>
                        </div>
                         </div>
                     </div>
              
              </div>
             
         <div class="panel-footer">
			 <div class="row">
				<div class="col-sm-12">
				  <button type="submit" class="btn btn-primary">Valider</button>&nbsp;
				  <button type="reset" class="btn btn-default">Annuler</button> &nbsp;
				  <a href="deleteModule?id=${moduleForm.id}" class="btn btn-danger delete">SUPPRIMER</a>
				</div>
			 </div>
		  </div><!-- panel-footer -->
		  </f:form>
     </div>
 </div>
<jsp:include page="../../../views/layout/rightpanel.jsp" />
<jsp:include page="../../../views/layout/footer.jsp">
	<jsp:param name="javascripts" value="/assets/js/chosen.jquery.min.js" />
</jsp:include>


<script>
  jQuery(document).ready(function() {

    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");
    
    jQuery('.datepicker-multiplee').datepicker({
        numberOfMonths: 3,
        showButtonPanel: true
      });
    
    jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
    
    jQuery("a.delete").click(function(e){
  	  e.preventDefault();
  	  var url = $(this).attr("href");
      swal(
      {
          title: "Êtes-vous sure?",
          text: "Vous ne serez pas en mesure de récupérer cet élément",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Oui, supprimez-le!",
          cancelButtonText: "Non, annuler!",
          closeOnConfirm: false,
          closeOnCancel: false
      },
      function(isConfirm)
      {
          if (isConfirm) {
          	window.location = url;
          	swal("Suppression!", "L'élement va être supprimé dans quelques instants.", "success");
          }
          else {
              swal("Annulé", "Aucune opération n'a été effectuer", "error");
          }
      });
      
      return false;
    });
 

  });
</script>

</body>
</html>