<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
  <div class="mainpanel">
    
    <div class="headerbar">
      
      <a class="menutoggle"><i class="fa fa-bars"></i></a>
      
      
      <div class="header-right">
        <ul class="headermenu">
          
          <li>
            <div class="btn-group">
              <button class="btn btn-default dropdown-toggle tp-icon" data-toggle="dropdown">
                <i class="glyphicon glyphicon-envelope"></i>
                <span class="badge hidden" id="top_total_messages">0</span>
              </button>
              <div class="dropdown-menu dropdown-menu-head pull-right">
                <h5 class="title">Vous avez <span id="top_total_messages_next">0</span> nouveaux messages</h5>
                <ul class="dropdown-list gen-list" id="messages_list_top">
                  
                  
                 
                </ul>
              </div>
            </div>
          </li>
          
          <li>
            <div class="btn-group">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                <img src="<c:url value="/assets/images/photos/loggeduser.png" />" alt="" />
                <sec:authentication property="principal.username" />
                <span class="caret"></span>
              </button>
              <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                <li><a href="<c:url value="/compte/parametrage" />"><i class="glyphicon glyphicon-cog"></i> Paramètres</a></li>
                <li><a href="<c:url value="/help" />"><i class="glyphicon glyphicon-question-sign"></i> Aide</a></li>
                <li><a href="<c:url value="/j_spring_security_logout"/>"><i class="glyphicon glyphicon-log-out"></i> Déconnexion</a></li>
              
              </ul>
            </div>
          </li>
          
        </ul>
      </div><!-- header-right -->
      
    </div><!-- headerbar -->