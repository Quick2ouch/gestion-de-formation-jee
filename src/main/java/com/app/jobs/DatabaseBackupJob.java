package com.app.jobs;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;
import java.io.InputStream;
import java.util.Properties;

import javassist.bytecode.stackmap.TypeData.ClassName;

public class DatabaseBackupJob {

	
	private static final Logger log = Logger.getLogger( ClassName.class.getName() );
	private Properties properties;
	public void startBackup() {
		log.info("Backing up database");
		// Get the inputStream
        InputStream inputStream = this.getClass().getClassLoader()
                .getResourceAsStream("backup.properties");
        properties = new Properties();

        System.out.println("InputStream is: " + inputStream);
        
     // load the inputStream using the Properties
        try {
			properties.load(inputStream);
			start();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
        
	}
	
	public void start(){

        /*NOTE: Creating Path Constraints for backup saving*/
        /*NOTE: Here the backup is saved in a folder called backup with the name backup.sql*/
         String savePath = properties.getProperty("backupFolderPath") + "/";
         
         
		this.backupDataWithOutDatabase(
				properties.getProperty("host"), 
				properties.getProperty("port"), 
				properties.getProperty("username"), 
				properties.getProperty("password"), 
				properties.getProperty("database"), 
				savePath
		);
	}

	public boolean backupDataWithOutDatabase(String host,
			String port, String user, String password, String database,
			String backupPath) {
		
		boolean status = false;
		
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date date = new Date();
		String filepath = "backup-" + database + "-" + host
				+ ""+ dateFormat.format(date) +".sql";
		
	
				ProcessBuilder pb = new ProcessBuilder("bash", "-c", "mysqldump -u" + user + " -p" + password + " " + database + " > " + backupPath + "" + filepath);
       try {
		Process process = pb.start();
		 final int exitValue = process.waitFor();
	        System.out.println("Process exit value = " + exitValue);
	        if (exitValue == 0) {
				status = true;
				log.info("Backup created successfully for DB "
						+ database + " in " + host + ":" + port);
			} else {
				status = false;
				log.info("Could not create the backup for DB "
						+ database + " in " + host + ":" + port);
			}
	        
		} catch (IOException | InterruptedException e) {
			System.out.println(e.getMessage());
		}

	
		return status;
	}

}
