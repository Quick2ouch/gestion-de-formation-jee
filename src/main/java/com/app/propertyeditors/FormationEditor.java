package com.app.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.app.model.Formation;

public class FormationEditor extends PropertyEditorSupport {
	
	// Converts a String to a Permission (when submitting form)
    @Override
    public void setAsText(String text) {
    	int id=Integer.valueOf(text);
    	Formation c = new Formation();
    	c.setId(id);
        this.setValue(c);
    }

    // Converts a Permission to a String (when displaying form)
    public String getAsText() {
    	if(getValue() == null){
    	    return null;
    	   }
    	Formation c = (Formation) this.getValue();
    	if (c != null)
            return c.getIntitule();
        return null;
    }

}
