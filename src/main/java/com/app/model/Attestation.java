package com.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.joda.time.DateTime;

@Entity
@DiscriminatorValue("attestation")
public class Attestation extends Document{
	@OneToMany(mappedBy = "attestation", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@OrderBy("field ASC")
    private Set<AttestationField> fields = new HashSet<AttestationField>();

	public Attestation() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public Attestation(int id, String intitule, String path,
			DateTime date_creation, Set<AttestationField> fields) {
		super(id, intitule, path, date_creation);
		this.fields = fields;
	}



	public Attestation(int id, String intitule, String path, DateTime date_creation) {
		super(id, intitule, path, date_creation);
		// TODO Auto-generated constructor stub
	}



	public Set<AttestationField> getFields() {
		return fields;
	}

	public void setFields(Set<AttestationField> fields) {
		this.fields = fields;
	}
	
	public void addField(AttestationField field) {
		  if (fields == null) {
		    fields = new HashSet<AttestationField>();
		  }
		  fields.add(field);
		  field.setAttestation(this);
	}
	
	
}
