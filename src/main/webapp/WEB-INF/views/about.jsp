<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<jsp:include page="/WEB-INF/views/layout/header.jsp">
	<jsp:param name="page_title" value="About Page" />
	<jsp:param name="about_menu" value="active" />
	<jsp:param name="stylesheets" value="login.css" />
	<jsp:param name="stylesheets" value="login2.css" />
</jsp:include>

<div class="jumbotron">
	<h1>Jumbotron heading</h1>
	<p class="lead">Cras justo odio, dapibus ac facilisis in, egestas
		eget quam. Fusce dapibus, tellus ac cursus commodo, tortor mauris
		condimentum nibh, ut fermentum massa justo sit amet risus.</p>
	<p>
		<a class="btn btn-lg btn-success" href="#" role="button">Sign up
			today</a>
	</p>
</div>

<div class="row marketing">
	<div class="col-lg-6">
		<h4>Subheading</h4>
		<p>Donec id elit non mi porta gravida at eget metus. Maecenas
			faucibus mollis interdum.</p>

		<h4>Subheading</h4>
		<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
			Cras mattis consectetur purus sit amet fermentum.</p>

		<h4>Subheading</h4>
		<p>Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
	</div>

	<div class="col-lg-6">
		<h4>Subheading</h4>
		<p>Donec id elit non mi porta gravida at eget metus. Maecenas
			faucibus mollis interdum.</p>

		<h4>Subheading</h4>
		<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros.
			Cras mattis consectetur purus sit amet fermentum.</p>

		<h4>Subheading</h4>
		<p>Maecenas sed diam eget risus varius blandit sit amet non magna.</p>
	</div>
</div>


<jsp:include page="/WEB-INF/views/layout/footer.jsp" />