package com.app.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.app.model.Enseignant;
import com.app.model.Salle;

public class SalleEditor extends PropertyEditorSupport {
	
	// Converts a String to a Permission (when submitting form)
	@Override
    public void setAsText(String text) {
    	int id=Integer.valueOf(text);
    	Salle c;
    	if(id == 0)
    		c = null;
    	else{
			c=  new Salle();
	    	c.setId(id);
    	}
        this.setValue(c);
    }

    // Converts a Permission to a String (when displaying form)
    public String getAsText() {
    	if(getValue() == null){
    	    return null;
    	   }
    	Salle c = (Salle) this.getValue();
    	if (c != null)
            return c.getIntitule();
        return null;
    }

}
