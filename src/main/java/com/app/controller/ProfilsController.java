package com.app.controller;

import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.model.Permission;
import com.app.model.Role;
import com.app.propertyeditors.PermissionEditor;
import com.app.service.LogTrackerService;
import com.app.service.PermissionService;
import com.app.service.RoleService;

@Controller
public class ProfilsController {
	@Autowired
	LogTrackerService logSvc;
	@Autowired
	RoleService roleService;
	
	@Autowired
	PermissionService permissionService;
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Permission.class, new PermissionEditor());
    }
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/profils", method = RequestMethod.GET)
	public String index(Model model) 
	{
		model.addAttribute("liste", roleService.getAll());
		return "parametrage/profils/liste";
	}
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/profils/ajouter", method = RequestMethod.GET)
	public String ajouter(Model model) 
	{
		model.addAttribute("roleForm", new Role());
		model.addAttribute("listePermissions", permissionService.getAll());
		return "parametrage/profils/ajouter";
	}
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/profils/save", method = RequestMethod.POST)
	public String save(
			@Valid @ModelAttribute("roleForm") Role p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			//System.out.println("Erreur");
			//System.out.println(bindingResult.toString());
			model.addAttribute("roleForm", p);
			model.addAttribute("listePermissions", permissionService.getAll());
			
			return "parametrage/profils/ajouter";
		} else {
			
			roleService.add(p);
			logSvc.store("Ajout Role: "+p, request);
			redirectAttributes.addFlashAttribute("success_profils", p.getName() + " a ete bien ajouter");
			return "redirect:/parametrage/profils/ajouter";

		}
	}
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/profils/edit", method = RequestMethod.GET)
	public String edit(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) {
		Role p = roleService.findById(id);
		System.out.println("Editing "+p);
		model.addAttribute("roleForm", p);
		model.addAttribute("listePermissions", permissionService.getAll());
		
		return "parametrage/profils/modifier";
	}
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/profils/update", method = RequestMethod.POST)
	public String update(
			@Valid @ModelAttribute("roleForm") Role p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			model.addAttribute("roleForm", p);
			model.addAttribute("listePermissions", permissionService.getAll());
			
			return "parametrage/profils/modifier";
		} else {
			
			try {
				roleService.update(p);
				logSvc.store("Mise a jour Role: "+p, request);
				redirectAttributes.addFlashAttribute("success_profils", p.getName() + " a ete bien modifier");
				return "redirect:/parametrage/profils/edit?id="+p.getId();
			} catch (NotFoundException e) {
				redirectAttributes.addFlashAttribute("error_profils", "Entity not found");
				return "redirect:/parametrage/profils/edit?id="+p.getId();
			}
			


		}
	}
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/profils/delete", method = RequestMethod.GET)
	public String delete(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		try {
			Role p = roleService.findById(id);
			roleService.delete(id);
			logSvc.store("Suppression Role: "+p, request);

			redirectAttributes.addFlashAttribute("success_profils_delete", p.getName()
					+ " a ete supprimer");

			return "redirect:/parametrage/profils";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_profile_delete", "Entity not found");
			return "redirect:/parametrage/profils";
		}
		
	}

}
