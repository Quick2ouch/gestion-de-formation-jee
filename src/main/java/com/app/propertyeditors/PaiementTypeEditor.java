package com.app.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.app.enums.PaiementType;



public class PaiementTypeEditor extends PropertyEditorSupport {
	
	// Converts a String to a Permission (when submitting form)
    @Override
    public void setAsText(String text) {
        this.setValue(PaiementType.valueOf(text));
    }

    // Converts a Permission to a String (when displaying form)
    public String getAsText() {
    	if(getValue() == null){
    	    return null;
    	   }
    	PaiementType c = (PaiementType) this.getValue();
    	if (c != null)
            return c.name();
        return null;
    }

}
